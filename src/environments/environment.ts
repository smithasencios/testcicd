// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  config: {
    apiKey: "AIzaSyDw_2nDc6EKAC_85x54ua0epQo9OzVP3GY",
    authDomain: "cdtest-cab0e.firebaseapp.com",
    databaseURL: "https://cdtest-cab0e.firebaseio.com",
    projectId: "cdtest-cab0e",
    storageBucket: "cdtest-cab0e.appspot.com",
    messagingSenderId: "223737419597"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
